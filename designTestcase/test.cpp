#include <iostream>
#include "autograder.hpp"

int main()
{
    Autograder autograder;
    autograder.runAutograder();

    /* **************** TASK 2&3  BUILD TESTCASE ******************* *
     * In this section, you will design your own testcase for        *
     * 'BasicMove' function in game 2048.                            *
     *                                                               *
     * When you finish building testcase, run                        *
     *                                                               *
     *     g++ test.cpp autograder.o -o test                         *
     *                                                               *
     * Then run 'test'(on Linux) or 'test.exe'(on Windows) to check  *
     * the result.                                                   *
     *                                                               *
     * You can change the template testcases if you want.            *
     *                                                               *
     * ************************************************************* */

    // Testcase 1
    {
        int testcase[][LINE_LENGTH] = {  {2, 4, 4, 0},      // This is the INPUT
                                         {2, 8, 0, 0}  };   // This is the expected OUTPUT
        autograder.test(testcase);
    }

    // Testcase 2
    {
        int testcase[][LINE_LENGTH] = {  {0, 0, 0, 0},      // This is the INPUT
                                         {0, 0, 0, 0}  };   // This is the expected OUTPUT
        autograder.test(testcase);
    }

    // Testcase 3
    {
        int testcase[][LINE_LENGTH] = {  {2, 0, 0, 0},      // This is the INPUT
                                         {2, 0, 0, 0}  };   // This is the expected OUTPUT
        autograder.test(testcase);
    }

    // You can add more testcases below.
    
    // YOUR CODE HERE

    return 0;
}